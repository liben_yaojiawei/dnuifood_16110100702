package cn.dnui_yjw702.dnuifood_16110100702.iface;

import java.util.List;

import cn.dnui_yjw702.dnuifood_16110100702.bean.CommentsEntity;
import cn.dnui_yjw702.dnuifood_16110100702.bean.Result;
import cn.dnui_yjw702.dnuifood_16110100702.service.RetrofitListener;

public interface OrderIface {

    void getAllOrdersByUser(Integer user_id, RetrofitListener<List<CommentsEntity>> m);

    void insertOrder(Integer user_id,
                     Integer food_id,
                     Integer num,
                     Double sum,
                     String suggesttime,
                     String address,
                     final RetrofitListener<Result> m);


    void insertComment(String item_id,
                       String content,
                       final RetrofitListener<Result> m);

    void getAllCommentsByUser(Integer user_id, RetrofitListener<List<CommentsEntity>> m);


    void updateComment(String item_id,
                       String content, final RetrofitListener<Result> m);

    void deleteComment(String item_id, final RetrofitListener<Result> m);
}
