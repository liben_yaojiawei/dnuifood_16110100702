package cn.dnui_yjw702.dnuifood_16110100702.iface;

import java.util.List;

import cn.dnui_yjw702.dnuifood_16110100702.bean.CollectStatus;
import cn.dnui_yjw702.dnuifood_16110100702.bean.IsCollection;
import cn.dnui_yjw702.dnuifood_16110100702.bean.Result;
import cn.dnui_yjw702.dnuifood_16110100702.service.RetrofitListener;

public interface CollectionIface {

    void getAllUserCollection(Integer user_id, Integer flag, RetrofitListener<List<CollectStatus>> mCallback);

    void isCollected(Integer user_id, Integer shop_food_id, Integer flag, RetrofitListener<IsCollection> mCallback);

    void userCollectFood(Integer user_id, Integer food_id, RetrofitListener<Result> mCallback);

    void userCollectShop(Integer user_id, Integer shop_id, RetrofitListener<Result> mCallback);
}
