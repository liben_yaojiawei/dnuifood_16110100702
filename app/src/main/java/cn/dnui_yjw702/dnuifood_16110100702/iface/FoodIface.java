package cn.dnui_yjw702.dnuifood_16110100702.iface;

import java.util.List;


import cn.dnui_yjw702.dnuifood_16110100702.bean.CommentsEntity;
import cn.dnui_yjw702.dnuifood_16110100702.bean.FoodMenu;
import cn.dnui_yjw702.dnuifood_16110100702.bean.Shop;
import cn.dnui_yjw702.dnuifood_16110100702.service.RetrofitListener;

public interface FoodIface {

    /*


    shop_id:店铺 ID
shopname:店铺名称
address:店铺地址
phonenum:订餐电话
intro:店铺简介
pic:店铺图片
comment:备注
level:等级

     */
    void getAllShops(RetrofitListener<List<Shop>> out);

    void getFoodBySearch(String kw, RetrofitListener<List<FoodMenu>> out);

    void getAllCommentsByFood(String food_id, RetrofitListener<List<CommentsEntity>> mCallback);

    void getFoodById(Integer food_id, RetrofitListener<FoodMenu> menuMCallback);
}
