package cn.dnui_yjw702.dnuifood_16110100702.iface;

import java.util.List;

import cn.dnui_yjw702.dnuifood_16110100702.bean.FoodMenu;
import cn.dnui_yjw702.dnuifood_16110100702.service.RetrofitListener;

public interface ShopIface {

    void getFoodByShop(Integer shop_id, RetrofitListener<List<FoodMenu>> out);

}
