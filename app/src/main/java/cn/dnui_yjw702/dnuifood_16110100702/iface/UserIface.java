package cn.dnui_yjw702.dnuifood_16110100702.iface;

import cn.dnui_yjw702.dnuifood_16110100702.bean.Result;
import cn.dnui_yjw702.dnuifood_16110100702.bean.User;
import cn.dnui_yjw702.dnuifood_16110100702.bean.LoginBean;
import cn.dnui_yjw702.dnuifood_16110100702.service.RetrofitListener;

public interface UserIface {

    void login(String username,
               String password,
               RetrofitListener<LoginBean> callback);


    void register(String username,
                  String password,
                  String phone,
                  String address,
                  String comment,
                  RetrofitListener<Result> out);

    void getUser(Integer user_id, RetrofitListener<User> out);

    void updateUserById(Integer user_id,
                        String username,
                        String userpass,
                        String mobilenum,
                        String address,
                        RetrofitListener<Result> out);
}
