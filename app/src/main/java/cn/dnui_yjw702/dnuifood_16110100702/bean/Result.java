package cn.dnui_yjw702.dnuifood_16110100702.bean;

public class Result {
    private Integer success;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }
}
