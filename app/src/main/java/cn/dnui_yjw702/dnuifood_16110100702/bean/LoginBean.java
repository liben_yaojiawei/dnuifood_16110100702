package cn.dnui_yjw702.dnuifood_16110100702.bean;

public class LoginBean {
    String userid;

    public LoginBean(){}
    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    @Override
    public String toString() {
        return "LoginBean{" +
                "userid='" + userid + '\'' +
                '}';
    }
}