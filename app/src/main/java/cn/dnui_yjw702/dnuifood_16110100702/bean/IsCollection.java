package cn.dnui_yjw702.dnuifood_16110100702.bean;

public class IsCollection {
    private Integer collected;

    public Integer getCollected() {
        return collected;
    }

    public void setCollected(Integer collected) {
        this.collected = collected;
    }
}
