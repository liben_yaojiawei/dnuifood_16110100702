package cn.dnui_yjw702.dnuifood_16110100702.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import cn.dnui_yjw702.dnuifood_16110100702.MyCompent.TitleView;
import cn.dnui_yjw702.dnuifood_16110100702.R;
import cn.dnui_yjw702.dnuifood_16110100702.adapter.FoodListAdapter;
import cn.dnui_yjw702.dnuifood_16110100702.bean.FoodMenu;
import cn.dnui_yjw702.dnuifood_16110100702.bean.IsCollection;
import cn.dnui_yjw702.dnuifood_16110100702.bean.Result;
import cn.dnui_yjw702.dnuifood_16110100702.model.CollectionModel;
import cn.dnui_yjw702.dnuifood_16110100702.model.ShopModel;
import cn.dnui_yjw702.dnuifood_16110100702.service.RetrofitListener;
import cn.dnui_yjw702.dnuifood_16110100702.bean.Store;

import java.util.ArrayList;
import java.util.List;

public class FoodListActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "FoodListActivity";
    CollectionModel model = new CollectionModel();
    private Intent intent;
    private Integer shop_id;
    private RecyclerView recyclerView;
    private List<FoodMenu> foodMenus;
    private FoodListAdapter foodListAdapter;
    private LinearLayoutManager layoutManager;
    private TextView collect;
    private String userId, shopName;
    private Context context;
    private TitleView titleView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_detail);
        init();
        loadData();
    }

    public void init() {
        context = this;
        titleView = findViewById(R.id.title_activity);
        collect = titleView.getRightTextTv();
        collect.setOnClickListener(this);
        //访问后成功后获得数据
        foodMenus = new ArrayList<>();
        intent = getIntent();
        shopName = intent.getStringExtra("shopName");//接收传递的shopname
        Store.shopName = shopName;
        shop_id = intent.getIntExtra("shop_id", 0);//接收传递的shopid
        userId = getSharedPreferences("userConfig", MODE_PRIVATE)
                .getString("userid", "0");

        layoutManager = new LinearLayoutManager(this);
        recyclerView = findViewById(R.id.foodMenu);
        recyclerView.setLayoutManager(layoutManager);
        //加载数据
        foodListAdapter = new FoodListAdapter(foodMenus);
        recyclerView.setAdapter(foodListAdapter);


        //判断收藏

        final String userId = getSharedPreferences("userConfig", Context.MODE_PRIVATE)
                .getString("userid", "");

        model.isCollected(Integer.parseInt(userId), shop_id, 0, new RetrofitListener<IsCollection>() {
            @Override
            public void onSuccess(IsCollection data) {

                if (data.getCollected() == 1) {
                    collect.setText("已收藏");
                } else {
                    collect.setText("未收藏");
                }
            }

            @Override
            public void onFailure(String error) {
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void loadData() {

        ShopModel shopModel = new ShopModel();
        shopModel.getFoodByShop(shop_id, new RetrofitListener<List<FoodMenu>>() {
            @Override
            public void onSuccess(List<FoodMenu> data) {
                foodMenus.clear();
                foodMenus.addAll(data);
                foodListAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(String error) {

            }
        });

    }

    @Override
    public void onClick(View v) {
        model.userCollectShop(Integer.parseInt(userId), shop_id, new RetrofitListener<Result>() {
            @Override
            public void onSuccess(Result data) {
                if (data.getSuccess() == 1) {

                    if (collect.getText().equals("未收藏")) {
                        collect.setText("已收藏");
                        Toast.makeText(context, "收藏成功", Toast.LENGTH_SHORT).show();
                    } else {
                        collect.setText("未收藏");
                        Toast.makeText(context, "取消成功", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, "操作失败", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(String error) {
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
            }
        });
    }


}
