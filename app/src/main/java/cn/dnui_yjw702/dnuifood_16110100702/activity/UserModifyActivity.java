package cn.dnui_yjw702.dnuifood_16110100702.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import cn.dnui_yjw702.dnuifood_16110100702.R;
import cn.dnui_yjw702.dnuifood_16110100702.bean.Store;
import cn.dnui_yjw702.dnuifood_16110100702.bean.Result;
import cn.dnui_yjw702.dnuifood_16110100702.model.UserModel;
import cn.dnui_yjw702.dnuifood_16110100702.service.RetrofitListener;

public class UserModifyActivity extends BaseActivity implements View.OnClickListener{

    private Button submit;
    private TextView username;
    private EditText password,phone,address;
    private ImageView userimg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_modify);
        init();
    }

    private Context context;
    private void init() {


        context = this;
        submit = findViewById(R.id.submit);
        submit.setOnClickListener(this);
        username = findViewById(R.id.username);
        username.setText(Store.user.getUsername());
        password = findViewById(R.id.password);

        phone = findViewById(R.id.phone);
        phone.setText(Store.user.getMobilenum());
        address = findViewById(R.id.address);
        address.setText(Store.user.getAddress());

        userimg=findViewById(R.id.userImg);
        userimg.setOnClickListener(this);
    }

    private static final String TAG = "UserModifyActivity";
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.submit:

                if (password.getText().toString().equals("")){
                    Toast.makeText(this,"请输入密码",Toast.LENGTH_SHORT).show();
                    return;
                }

                UserModel userModel = new UserModel();
                userModel.updateUserById(Integer.parseInt(Store.userId), username.getText().toString(), password.getText().toString(), phone.getText().toString(), address.getText().toString(), new RetrofitListener<Result>() {
                    @Override
                    public void onSuccess(Result data) {
                        if(data==null){
                            return;
                        }
                        if(data.getSuccess()==1){
                            Toast.makeText(context,"修改成功",Toast.LENGTH_SHORT).show();
                            new MainActivity().getUser();
                            finish();
                        }else{
                            Toast.makeText(context,"修改失败",Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(String error) {

                    }
                });
                break;
            case  R.id.userImg:
                Intent intent=new Intent(UserModifyActivity.this,PhotoActivity.class);
                startActivityForResult(intent,1);
                break;
        }
    }


    }
