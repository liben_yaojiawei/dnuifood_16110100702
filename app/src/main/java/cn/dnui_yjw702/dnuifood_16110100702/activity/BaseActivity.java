package cn.dnui_yjw702.dnuifood_16110100702.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

public abstract class BaseActivity extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityCollertor.addActivity(this);//向List添加活动
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ActivityCollertor.removeActivity(this);//从List移除活动
    }
}
