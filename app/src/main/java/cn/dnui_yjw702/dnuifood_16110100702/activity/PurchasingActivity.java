package cn.dnui_yjw702.dnuifood_16110100702.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;



import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cn.dnui_yjw702.dnuifood_16110100702.R;
import cn.dnui_yjw702.dnuifood_16110100702.adapter.TimeAdapter;
import cn.dnui_yjw702.dnuifood_16110100702.bean.FoodMenu;
import cn.dnui_yjw702.dnuifood_16110100702.bean.Store;
import cn.dnui_yjw702.dnuifood_16110100702.model.FoodModel;
import cn.dnui_yjw702.dnuifood_16110100702.service.RetrofitListener;

public class PurchasingActivity extends BaseActivity implements View.OnClickListener {

    private Spinner spinner;
    private List<String> datas;
    private TimeAdapter timeAdapter;
    private FoodMenu foodMenu;
    private TextView username, price, foodName, foodPrice, buyTime, shopName, sum;
    private EditText inputAddress, num;
    private Button reduce, increase, submit, add;
    private Context context;
    private Integer food_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchasing);
        initCompont();
    }

    public void initCompont() {
        context = this;
        food_id = getIntent().getIntExtra("food_id",0);

        FoodModel model =new FoodModel();
        model.getFoodById(food_id, new RetrofitListener<FoodMenu>() {
            @Override
            public void onSuccess(FoodMenu data) {
                if(data!=null){
                    foodMenu = data;
                    username.setText(Store.username);
                    shopName.setText(Store.shopName);
                    foodName.setText(foodMenu.getFoodname());
                    foodPrice.setText(foodMenu.getPrice() + "");
                    price.setText("单价：" + foodMenu.getPrice());
                    num.setText("1");
                    sum.setText("总计：" + foodMenu.getPrice());
                    buyTime.setText("下单时间：" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                }else{
                    Toast.makeText(context,"获取失败",Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(String error) {
                Toast.makeText(context,"获取失败:"+error,Toast.LENGTH_SHORT).show();

            }
        });


        username = findViewById(R.id.username);

        inputAddress = findViewById(R.id.address);

        add = findViewById(R.id.add);
        add.setOnClickListener(this);

        shopName = findViewById(R.id.shopName);


        foodName = findViewById(R.id.foodName);


        foodPrice = findViewById(R.id.foodPrice);


        price = findViewById(R.id.price);


        reduce = findViewById(R.id.reduce);
        increase = findViewById(R.id.increase);

        num = findViewById(R.id.num);


        sum = findViewById(R.id.sum);


        num.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (num.getText().toString().equals("0")) {
                    num.setText("1");
                    Toast.makeText(PurchasingActivity.this, "数量必须不为零"
                            , Toast.LENGTH_SHORT).show();
                }
                try {
                    sum.setText("总计" + String.valueOf(foodMenu.getPrice() * Integer.parseInt(num.getText().toString())));
                } catch (Exception e) {

                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (num.getText().toString().equals("0")) {
                    num.setText("1");
                    Toast.makeText(PurchasingActivity.this, "数量必须不为零"
                            , Toast.LENGTH_SHORT).show();
                }
                try {
                    sum.setText("总计" + String.valueOf(foodMenu.getPrice() * Integer.parseInt(num.getText().toString())));
                } catch (Exception e) {

                }
            }
        });


        buyTime = findViewById(R.id.buy_time);

        reduce.setOnClickListener(this);
        increase.setOnClickListener(this);

        submit = findViewById(R.id.submit);
        submit.setOnClickListener(this);

        spinner = findViewById(R.id.spinner);

        datas = new ArrayList<>();
        for (int i = 1; i <= 24; ++i) {
            datas.add(i - 1 + ":05----" + (i - 1) + ":35");
            datas.add((i - 1) + ":35----" + (i == 24 ? 0 : i) + ":05");
        }
        timeAdapter = new TimeAdapter(this);
        spinner.setAdapter(timeAdapter);
        timeAdapter.setDatas(datas);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.reduce:
                if (num.getText().toString().equals("1")) {
                    return;
                }
                num.setText((Integer.parseInt(num.getText().toString()) - 1) + "");
                sum.setText("总计" + String.valueOf(foodMenu.getPrice() * Integer.parseInt(num.getText().toString())));
                break;
            case R.id.increase:
                num.setText((Integer.parseInt(num.getText().toString()) + 1) + "");
                sum.setText("总计" + String.valueOf(foodMenu.getPrice() * Integer.parseInt(num.getText().toString())));
                break;
            case R.id.submit:

                Log.d(TAG, "onClick: " + String.valueOf(foodMenu.getPrice() * Integer.parseInt(num.getText().toString())));
                startActivity(new Intent(this, Payment.class)
                        .putExtra("price", foodMenu.getPrice())
                        .putExtra("sum", foodMenu.getPrice() * Integer.parseInt(num.getText().toString()))
                        .putExtra("foodname", foodMenu.getFoodname())
                        .putExtra("food_id", foodMenu.getFood_id())
                        .putExtra("address", inputAddress.getText().toString())
                        .putExtra("number", num.getText().toString()).putExtra("suggessttime", datas.get(spinner.getSelectedItemPosition())));
                break;
            case R.id.add:
                break;
        }
    }

    private static final String TAG = "PurchasingActivity";
}
