package cn.dnui_yjw702.dnuifood_16110100702.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import cn.dnui_yjw702.dnuifood_16110100702.R;

public class SettingActivity extends BaseActivity implements View.OnClickListener{

    private TextView login,connect;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        init();
    }

    public void init(){
        login = findViewById(R.id.login_setting);
        login.setOnClickListener(this);
        connect = findViewById(R.id.connect_customerService);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.login_setting:
                ActivityCollertor.finishAll();
                startActivity(new Intent(this,LoginActivity.class));
                break;
        }
    }
}
