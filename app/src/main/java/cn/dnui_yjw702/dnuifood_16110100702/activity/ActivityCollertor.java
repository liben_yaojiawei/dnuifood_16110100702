package cn.dnui_yjw702.dnuifood_16110100702.activity;

import android.app.Activity;

import java.util.ArrayList;
import java.util.List;
//活动管理器
public abstract class ActivityCollertor {
    private static List<Activity> activities = new ArrayList<>();
    public static void addActivity(Activity activity){
        activities.add(activity);
    }

    public static void removeActivity(Activity activity){
        activities.remove(activity);
    }

    //测试finishAll()方法在ThirdActivity中
    public static void finishAll(){
        for (Activity activity:activities){
            if(!activity.isFinishing())
                activity.finish();
        }
    }

}
