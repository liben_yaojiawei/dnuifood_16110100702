package cn.dnui_yjw702.dnuifood_16110100702.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;



import cn.dnui_yjw702.dnuifood_16110100702.R;
import cn.dnui_yjw702.dnuifood_16110100702.bean.Result;
import cn.dnui_yjw702.dnuifood_16110100702.bean.Store;
import cn.dnui_yjw702.dnuifood_16110100702.model.OrderModel;
import cn.dnui_yjw702.dnuifood_16110100702.service.RetrofitListener;

public class Payment extends BaseActivity implements View.OnClickListener {

    OrderModel model = new OrderModel();
    private String foodName, number, suggesttime, address;
    private double price, sum;
    private TextView foodNameTv, priceTv, sumTv;
    private Intent intent;
    private int food_id;
    private Button payment;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        context = this;
        initCompent();
    }

    private void initCompent() {
        intent = getIntent();
        foodName = intent.getStringExtra("foodname");
        price = intent.getDoubleExtra("price", 0);
        sum = intent.getDoubleExtra("sum", 0);
        food_id = intent.getIntExtra("food_id", 0);
        number = intent.getStringExtra("number");
        suggesttime = intent.getStringExtra("suggessttime");
        address = intent.getStringExtra("address");
        foodNameTv = findViewById(R.id.foodName);
        priceTv = findViewById(R.id.price);
        sumTv = findViewById(R.id.sum);

        payment = findViewById(R.id.payment);
        payment.setOnClickListener(this);

        foodNameTv.setText(foodName);
        priceTv.setText(price + "");
        sumTv.setText("总计：" + sum);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.payment:

                model.insertOrder(Integer.parseInt(Store.userId), food_id, Integer.parseInt(number), sum, suggesttime, address, new RetrofitListener<Result>() {
                            @Override
                            public void onSuccess(Result data) {
                                if (data.getSuccess() == 1) {
                                    Toast.makeText(context, "购买成功", Toast.LENGTH_SHORT).show();
                                    finish();
                                } else {
                                    Toast.makeText(context, "购买失败", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(String error) {
                                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
                            }
                        }
                );

        }
    }
}