package cn.dnui_yjw702.dnuifood_16110100702.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import cn.dnui_yjw702.dnuifood_16110100702.MyCompent.TitleView;
import cn.dnui_yjw702.dnuifood_16110100702.R;

import cn.dnui_yjw702.dnuifood_16110100702.adapter.CommentAdapter;
import cn.dnui_yjw702.dnuifood_16110100702.bean.CommentsEntity;
import cn.dnui_yjw702.dnuifood_16110100702.bean.FoodMenu;
import cn.dnui_yjw702.dnuifood_16110100702.bean.IsCollection;
import cn.dnui_yjw702.dnuifood_16110100702.bean.Result;
import cn.dnui_yjw702.dnuifood_16110100702.model.CollectionModel;
import cn.dnui_yjw702.dnuifood_16110100702.model.FoodModel;
import cn.dnui_yjw702.dnuifood_16110100702.service.RetrofitListener;
import cn.dnui_yjw702.dnuifood_16110100702.MyCompent.Url;

import java.util.ArrayList;
import java.util.List;

public class FoodDetailActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "FoodDetailActivity";
    private Intent intent;
    private FoodMenu foodMenu;
    private ImageView foodImage;
    private TextView foodPrice, foodIntro, foodCollect;
    private RecyclerView recyclerView;
    private Button buy;
    private TitleView titleView;
    private String userId;
    private CommentAdapter adapter;
    private LinearLayoutManager layoutManager;
    private String shopId, foodId;
    private Context context;
    private List<CommentsEntity> comment;

    CollectionModel collectionModel = new CollectionModel();
    FoodModel model = new FoodModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_detail);

        init();

        Log.d(TAG, "onCreate: " + foodMenu.getFoodname());
    }

    @SuppressLint("SetTextI18n")
    public void init() {
        context = this;
        intent = getIntent();
        foodMenu = (FoodMenu) intent.getSerializableExtra("food");//接收
        shopId = foodMenu.getShop_id() + "";
        foodId = foodMenu.getFood_id() + "";
        userId = getSharedPreferences("userConfig", MODE_PRIVATE)
                .getString("userid", "0");

        buy = findViewById(R.id.buy);
        buy.setOnClickListener(this);

        foodImage = findViewById(R.id.foodImageDetail);
        foodPrice = findViewById(R.id.foodPriceDetail);
        foodIntro = findViewById(R.id.foodIntroDetail);

        recyclerView = findViewById(R.id.commentlist);//布局id
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);//设置动画

        comment = new ArrayList<>();
        adapter = new CommentAdapter(comment);//实例化
        recyclerView.setAdapter(adapter);//设置适配器

        foodIntro.setText(foodMenu.getIntro());
        Glide.with(this).load(Url.imgBase + foodMenu.getPic()).into(foodImage);
        foodPrice.setText("￥" + foodMenu.getPrice());

        titleView = findViewById(R.id.title_activity);
        foodCollect = titleView.getRightTextTv();//获取titleview的方法
        foodCollect.setOnClickListener(this);

        collectionModel.isCollected(Integer.parseInt(userId), Integer.parseInt(foodId), 1, new RetrofitListener<IsCollection>() {
            @Override
            public void onSuccess(IsCollection data) {
                if (data.getCollected() == 1) {
                    foodCollect.setText("已收藏");
                } else {
                    foodCollect.setText("未收藏");
                }
            }

            @Override
            public void onFailure(String error) {
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
            }
        });

        loadData();
    }

    public void loadData() {
        //foodmodel 获取评论
        model.getAllCommentsByFood(foodId, new RetrofitListener<List<CommentsEntity>>() {
            @Override
            public void onSuccess(List<CommentsEntity> data) {

                comment.clear();
                comment.addAll(data);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(String error) {
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.buy:
                //跳转
                startActivity(new Intent(this, PurchasingActivity.class).putExtra("food_id", foodMenu.getFood_id()));
                break;
            case R.id.add:

                break;
            default://获取收藏
                collectionModel.userCollectFood(Integer.parseInt(userId), Integer.parseInt(foodId), new RetrofitListener<Result>() {
                    @Override
                    public void onSuccess(Result data) {
                        if (data.getSuccess() == 1) {
                            if (foodCollect.getText().equals("未收藏")) {
                                foodCollect.setText("已收藏");
                                Toast.makeText(context,"收藏成功",Toast.LENGTH_SHORT).show();
                            } else {
                                foodCollect.setText("未收藏");
                                Toast.makeText(context,"取消收藏",Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(String error) {
                        Toast.makeText(context,error,Toast.LENGTH_SHORT).show();
                    }
                });

                break;
        }
    }
}
