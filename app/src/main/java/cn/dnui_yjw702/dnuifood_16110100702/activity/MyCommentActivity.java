package cn.dnui_yjw702.dnuifood_16110100702.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import cn.dnui_yjw702.dnuifood_16110100702.R;
import cn.dnui_yjw702.dnuifood_16110100702.adapter.MyCommentManagerAdapter;
import cn.dnui_yjw702.dnuifood_16110100702.bean.CommentsEntity;
import cn.dnui_yjw702.dnuifood_16110100702.bean.Store;
import cn.dnui_yjw702.dnuifood_16110100702.model.OrderModel;
import cn.dnui_yjw702.dnuifood_16110100702.service.RetrofitListener;

public class MyCommentActivity extends BaseActivity {

    private RecyclerView recyclerView;
    private MyCommentManagerAdapter adapter;
    private List<CommentsEntity> orders;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment_manage);
        context = this;
        orders = new ArrayList<>();
        recyclerView = findViewById(R.id.recy);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new MyCommentManagerAdapter(orders);
        recyclerView.setAdapter(adapter);
        loadDate();
    }

    private void loadDate() {

        OrderModel model = new OrderModel();

        model.getAllCommentsByUser(Integer.parseInt(Store.userId), new RetrofitListener<List<CommentsEntity>>() {
            @Override
            public void onSuccess(List<CommentsEntity> data) {
                if (data != null) {
                    orders.clear();
                    orders.addAll(data);
                    adapter.notifyDataSetChanged();
                }
                if (data.size() == 0) {
                    Toast.makeText(context, "空数据", Toast.LENGTH_SHORT).show();
                }


            }


            @Override
            public void onFailure(String error) {
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
            }
        });

    }

}
