package cn.dnui_yjw702.dnuifood_16110100702.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import cn.dnui_yjw702.dnuifood_16110100702.R;
import cn.dnui_yjw702.dnuifood_16110100702.bean.LoginBean;
import cn.dnui_yjw702.dnuifood_16110100702.bean.Store;
import cn.dnui_yjw702.dnuifood_16110100702.model.UserModel;
import cn.dnui_yjw702.dnuifood_16110100702.service.RetrofitListener;

public class LoginActivity extends BaseActivity implements View.OnClickListener{
    private EditText username_et,password_et;
    private CheckBox isRemember;
    private Button login_bt;
    private TextView newUserRegister;
    private static final String TAG = "LoginActivity";
    private Context context;
    private SharedPreferences.Editor editor;    //本地简单存储

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context=this;
        init();
    }

    public void init(){
        editor=getSharedPreferences("userConfig",MODE_PRIVATE).edit();

        username_et=findViewById(R.id.login_username);
        password_et=findViewById(R.id.login_password);
        isRemember=findViewById(R.id.rememberUsername);

        String userstr = getSharedPreferences("userConfig",MODE_PRIVATE).getString("username","");
        username_et.setText(userstr);

        if("".equals(userstr)){
            isRemember.setChecked(false);
        }else{
            isRemember.setChecked(true);
        }

        isRemember.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked){
                    Log.d(TAG, "onCheckedChanged: "+username_et.getText().toString());
                    editor.putString("username",username_et.getText().toString()).apply(); //存入用户名
                }
                else{
                    editor.remove("username").commit();
                }
            }
        });

        login_bt=findViewById(R.id.login);
        newUserRegister=findViewById(R.id.register);
        newUserRegister.setOnClickListener(this);
        login_bt.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.login:
                login();
                break;

            case R.id.register:
                startActivity(new Intent(this, RegisterActivity.class));
                break;

            default:
                break;
        }
    }

    public void login(){
        UserModel model = new UserModel();
        model.login(username_et.getText().toString(),password_et.getText().toString(),mCallback);
    }

    RetrofitListener<LoginBean> mCallback = new RetrofitListener<LoginBean>() {
        @Override
        public void onSuccess(LoginBean userId) {
            Log.d(TAG, "onSuccess: " +userId);

            if(userId.getUserid().equals("0")){
                Toast.makeText(LoginActivity.this,"登录失败",Toast.LENGTH_SHORT).show();
            }else{

                context.startActivity(new Intent(LoginActivity.this,MainActivity.class));
                finish();
                Toast.makeText(LoginActivity.this,"登录成功",Toast.LENGTH_SHORT).show();

                editor.putString("username",username_et.getText().toString()).apply();
                Store.username=username_et.getText().toString();
                Store.userId=userId.getUserid();
                Log.d(TAG, "run: " +userId.getUserid());

                editor.putString("userid",userId.getUserid()).commit();

            }

        }

        @Override
        public void onFailure(String error) {
            Toast.makeText(context,error,Toast.LENGTH_SHORT).show();
        }
    };
}
