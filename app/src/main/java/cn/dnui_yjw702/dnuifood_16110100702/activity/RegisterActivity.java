package cn.dnui_yjw702.dnuifood_16110100702.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import cn.dnui_yjw702.dnuifood_16110100702.R;
import cn.dnui_yjw702.dnuifood_16110100702.bean.Result;
import cn.dnui_yjw702.dnuifood_16110100702.model.UserModel;
import cn.dnui_yjw702.dnuifood_16110100702.service.RetrofitListener;

public class RegisterActivity extends BaseActivity {
    private EditText username,userpass,mobilnum,address,comment,repass;
    private Button button,returnlogin;
    private Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initView();
    }

    private void initView() {
        button = findViewById(R.id.submit);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                register();
            }
        });
        returnlogin = findViewById(R.id.returnlogin);
        returnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        username = findViewById(R.id.username);
        userpass = findViewById(R.id.password);
        repass = findViewById(R.id.repassword);
        mobilnum = findViewById(R.id.phone);
        address = findViewById(R.id.address);
        comment = findViewById(R.id.comment);
        context = this;
    }

    private void register(){

        if ("".equals(username.getText().toString())||
                "".equals(userpass.getText().toString())||
                "".equals(mobilnum.getText().toString())||
                "".equals(address.getText().toString())||
                "".equals(comment.getText().toString())){
            Toast.makeText(context,"字段不能为空",Toast.LENGTH_SHORT).show();
            return;
        }
        if(userpass.getText().toString().equals(repass.getText().toString())){
            UserModel model  = new UserModel();
            model.register(username.getText().toString(),
                    userpass.getText().toString(),
                    mobilnum.getText().toString(),
                    address.getText().toString(),
                    comment.getText().toString(),
                    mCallback);
        }else {
            Toast.makeText(context,"两次密码输入不相同",Toast.LENGTH_SHORT).show();
        }

    }

    RetrofitListener<Result> mCallback = new RetrofitListener<Result>() {
        @Override
        public void onSuccess(Result data) {
            if (data.getSuccess()==0){
                Toast.makeText(context,"注册失败",Toast.LENGTH_SHORT).show();
            }else if (data.getSuccess()==1){
                Toast.makeText(context,"注册成功",Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFailure(String error) {
            Toast.makeText(context,error,Toast.LENGTH_SHORT).show();
        }
    };
}
