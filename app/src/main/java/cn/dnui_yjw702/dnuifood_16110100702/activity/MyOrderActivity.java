package cn.dnui_yjw702.dnuifood_16110100702.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import cn.dnui_yjw702.dnuifood_16110100702.R;
import cn.dnui_yjw702.dnuifood_16110100702.adapter.MyOrderAdapter;
import cn.dnui_yjw702.dnuifood_16110100702.bean.CommentsEntity;
import cn.dnui_yjw702.dnuifood_16110100702.bean.Store;
import cn.dnui_yjw702.dnuifood_16110100702.model.OrderModel;
import cn.dnui_yjw702.dnuifood_16110100702.service.RetrofitListener;

public class MyOrderActivity extends BaseActivity {

    private RecyclerView recyclerView;
    private MyOrderAdapter adapter;
    private List<CommentsEntity> orders;
    private Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        context = this;
        recyclerView = findViewById(R.id.order_list);
        orders = new ArrayList<>();
        adapter = new MyOrderAdapter(orders);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        loadData();
    }

    private void loadData() {
        OrderModel model =new OrderModel();
        model.getAllOrdersByUser(Integer.parseInt(Store.userId), new RetrofitListener<List<CommentsEntity>>() {
            @Override
            public void onSuccess(List<CommentsEntity> data) {
                if (data!=null&&data.size()!=0) {
                    orders.clear();
                    orders.addAll(data);
                    adapter.notifyDataSetChanged();
                }else{
                    Toast.makeText(context,"数据获取失败",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(String error) {
                Toast.makeText(context,error,Toast.LENGTH_SHORT).show();
            }
        });
    }
}
