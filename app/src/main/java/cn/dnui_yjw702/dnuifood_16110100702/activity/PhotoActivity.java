package cn.dnui_yjw702.dnuifood_16110100702.activity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;

import cn.dnui_yjw702.dnuifood_16110100702.R;
import cn.dnui_yjw702.dnuifood_16110100702.activity.BaseActivity;

public class PhotoActivity extends BaseActivity implements View.OnClickListener{
    private Button pai,bendi;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);
        pai=findViewById(R.id.pai);
        bendi=findViewById(R.id.bendi);
        pai.setOnClickListener(this);
        bendi.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.pai:

                break;
            case R.id.bendi:
                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, 2);

                break;
            case R.id.queding:
                finish();
                break;
        }
    }
}