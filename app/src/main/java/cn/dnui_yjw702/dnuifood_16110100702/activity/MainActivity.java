package cn.dnui_yjw702.dnuifood_16110100702.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.widget.Toast;

import com.ashokvarma.bottomnavigation.BottomNavigationBar;
import com.ashokvarma.bottomnavigation.BottomNavigationItem;


import cn.dnui_yjw702.dnuifood_16110100702.Fragment.CollectFragment;
import cn.dnui_yjw702.dnuifood_16110100702.Fragment.HomeFragment;

import cn.dnui_yjw702.dnuifood_16110100702.Fragment.SearchFrament;
import cn.dnui_yjw702.dnuifood_16110100702.Fragment.UserFragment;
import cn.dnui_yjw702.dnuifood_16110100702.R;
import cn.dnui_yjw702.dnuifood_16110100702.bean.Store;
import cn.dnui_yjw702.dnuifood_16110100702.bean.User;
import cn.dnui_yjw702.dnuifood_16110100702.model.UserModel;
import cn.dnui_yjw702.dnuifood_16110100702.service.RetrofitListener;

public class MainActivity extends BaseActivity implements BottomNavigationBar.OnTabSelectedListener {

    private BottomNavigationBar mBottomNavigationBar;
    private static final String TAG = "MainActivity";
    private Fragment mainFragment,collectFragment,searchFragment,meFragment;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager);
        context = this;
        getUser();
        initBottom();

    }
    //初始化加载底部栏
    public void initBottom(){

        mBottomNavigationBar = findViewById(R.id.bottom_navigation_bar);//实例底部栏

        mBottomNavigationBar.addItem(new BottomNavigationItem(R.mipmap.home, "首页"))
                .addItem(new BottomNavigationItem(R.mipmap.col, "收藏"))
                .addItem(new BottomNavigationItem(R.mipmap.search, "搜索"))
                .addItem(new BottomNavigationItem(R.mipmap.user, "我的"))//依次添加item,各自的icon和名称
                .setFirstSelectedPosition(0)//设置默认选择item
                .setTabSelectedListener(this)
                .initialise();//所有的设置需在调用该方法前完成
        //默认加载首页
        //替换containerViewId中的fragment实例，注意，它首先把containerViewId中所有fragment删除，然后再add进去当前的fragment
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.ll_content, new HomeFragment())
                .commit();
        //注意提交
    }

    //BottomNavigationBar的点击选择事件
    @Override
    public void onTabSelected(int position) {
        Log.d(TAG, "onTabSelected: "+position);
        //Fragment管理,getSupportFragmentManager()获取实例
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        switch (position) {
            case 0:
                if (mainFragment == null) {
                    mainFragment = new HomeFragment();
                }
                transaction.replace(R.id.ll_content, mainFragment);//replace是将容器里之前添加的View全部清除，然后再添加当前fragment View
                break;
            case 1:
                if(collectFragment ==null){
                    collectFragment = new CollectFragment();
                }
                transaction.replace(R.id.ll_content,collectFragment);
                break;
            case 2:
                if(searchFragment ==null){
                    searchFragment = new SearchFrament();
                }
                transaction.replace(R.id.ll_content,searchFragment);
                break;
            case 3:
                if(meFragment ==null){
                    meFragment = new UserFragment();
                }
                transaction.replace(R.id.ll_content,meFragment);
                break;
        }
        transaction.commit();
    }

    @Override
    public void onTabUnselected(int position) {

    }

    @Override
    public void onTabReselected(int position) {

    }

    public void getUser(){

        UserModel userModel = new UserModel();
        userModel.getUser(Integer.parseInt(Store.userId), new RetrofitListener<User>() {
            @Override
            public void onSuccess(User data) {
                if (data==null){
                    Toast.makeText(context,"获取用户失败",Toast.LENGTH_SHORT).show();
                    return;
                }
                Store.user = data;
            }
            @Override
            public void onFailure(String error) {
                Toast.makeText(context,error,Toast.LENGTH_SHORT).show();
            }
        });

    }
}