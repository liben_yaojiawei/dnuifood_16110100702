package cn.dnui_yjw702.dnuifood_16110100702.service;

import java.util.List;

import cn.dnui_yjw702.dnuifood_16110100702.bean.CommentsEntity;
import cn.dnui_yjw702.dnuifood_16110100702.bean.Result;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface OrderCall {
    @GET("getAllOrdersByUser.do")
    Call<List<CommentsEntity>> getAllOrdersByUser(@Query("user_id") Integer user_id);

    //http://172.24.10.175:8080/foodService/insertOrder.do?user_id=1&food_id=3&num=2&sum=34.2&suggesttime=11:0%200-11:30&address=%E5%A4%A7%E8%BF%9E

    @GET("insertOrder.do")
    Call<Result> insertOrder(@Query("user_id") Integer user_id,
                             @Query("food_id") Integer food_id,
                             @Query("num") Integer num,
                             @Query("sum") Double sum,
                             @Query("suggesttime") String suggesttime,
                             @Query("address") String address);

//insertComment.do?item_id=1&content=很好
    @GET("insertComment.do")
    Call<Result> insertComment(@Query("item_id") String item_id,
                               @Query("content") String content);

    //getAllCommentsByUser.do?user_id=1

    @GET("getAllCommentsByUser.do")
    Call<List<CommentsEntity>> getAllCommentsByUser(@Query("user_id") Integer user_id);

    //http://172.24.10.175:8080/foodService/updateComment.do?item_id=1&content=%E5%BE%88%E5%A5%BD

    @GET("updateComment.do")
    Call<Result> updateComment(@Query("item_id") String item_id,
                               @Query("content") String content);

    //http://172.24.10.175:8080/foodService/deleteComment.do

    @GET("deleteComment.do")
    Call<Result> deleteComment(@Query("item_id") String item_id);

}
