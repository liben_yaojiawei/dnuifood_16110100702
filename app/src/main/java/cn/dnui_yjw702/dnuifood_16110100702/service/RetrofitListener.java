package cn.dnui_yjw702.dnuifood_16110100702.service;

public interface RetrofitListener<T> {
    void onSuccess(T t);
    void onFailure(String error);
}
