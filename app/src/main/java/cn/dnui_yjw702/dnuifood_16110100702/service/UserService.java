package cn.dnui_yjw702.dnuifood_16110100702.service;

import cn.dnui_yjw702.dnuifood_16110100702.bean.LoginBean;
import cn.dnui_yjw702.dnuifood_16110100702.bean.Result;
import cn.dnui_yjw702.dnuifood_16110100702.bean.User;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface UserService {
    //登录接口
    @GET("userLogin.do")
    Call<LoginBean> login(@Query("username") String username,
                          @Query("userpass") String password);
    //注册接口
    @GET("userRegister.do")
    Call<Result> register(@Query("username") String username,
                          @Query("userpass") String userpass,
                          @Query("mobilenum") String mobilnum,
                          @Query("address") String address,
                          @Query("comment") String comment);

    @GET("getUserById.do")
    Call<User> getUser(@Query("user_id") Integer id);


    @GET("updateUserById.do")
    Call<Result> updateUserById(@Query("user_id") Integer user_id,
                                @Query("username") String username,
                                @Query("userpass") String userpass,
                                @Query("mobilenum") String mobilenum,
                                @Query("address") String address);
}
