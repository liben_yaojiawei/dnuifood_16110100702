package cn.dnui_yjw702.dnuifood_16110100702.service;

import java.util.List;


import cn.dnui_yjw702.dnuifood_16110100702.bean.CommentsEntity;
import cn.dnui_yjw702.dnuifood_16110100702.bean.FoodMenu;
import cn.dnui_yjw702.dnuifood_16110100702.bean.Shop;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ShopService {
    //店铺列表
    @GET("getAllShops.do")
    Call<List<Shop>> getAllShops();
    //菜谱列表
    @GET("getFoodByShop.do")
    Call<List<FoodMenu>> getFoodByShop(@Query("shop_id") Integer shop_id);
    //搜索
    @GET("getFoodBySearch.do")
    Call<List<FoodMenu> > getFoodBySearch(@Query("search") String search);
    //获取评论
    @GET("getAllCommentsByFood.do")
    Call<List<CommentsEntity>> getAllCommentsByFood(@Query("food_id") Integer food_id);
    //菜品详情
    @GET("getFoodById.do")
    Call<FoodMenu> getFoodById(@Query("food_id") Integer food_id);
}
