package cn.dnui_yjw702.dnuifood_16110100702.service;

import java.util.List;

import cn.dnui_yjw702.dnuifood_16110100702.bean.CollectStatus;
import cn.dnui_yjw702.dnuifood_16110100702.bean.IsCollection;
import cn.dnui_yjw702.dnuifood_16110100702.bean.Result;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface CollectService {

    //根据flag来区分标识收藏的是店铺（0）还是菜谱（1）
    @GET("getAllUserCollection.do")
    Call<List<CollectStatus>> getAllUserCollection(@Query("user_id") Integer user_id,
                                                   @Query("flag") Integer flag);
    //判断是否收藏
    @GET("isCollected.do")
    Call<IsCollection> isCollected(@Query("user_id") Integer user_id,
                                   @Query("shop_food_id") Integer shop_food_id,
                                   @Query("flag") Integer flag);

    //收藏菜品
    @GET("userCollectFood.do")
    Call<Result> userCollectFood(@Query("user_id") Integer user_id,
                                 @Query("food_id") Integer food_id);

    //收藏店铺
    @GET("userCollectShop.do")
    Call<Result> userCollectShop(@Query("user_id") Integer user_id,
                                 @Query("shop_id") Integer shop_id);

}
