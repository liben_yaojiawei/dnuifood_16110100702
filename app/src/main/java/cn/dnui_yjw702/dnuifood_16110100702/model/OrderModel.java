package cn.dnui_yjw702.dnuifood_16110100702.model;

import java.util.List;

import cn.dnui_yjw702.dnuifood_16110100702.bean.CommentsEntity;
import cn.dnui_yjw702.dnuifood_16110100702.bean.Result;
import cn.dnui_yjw702.dnuifood_16110100702.iface.OrderIface;
import cn.dnui_yjw702.dnuifood_16110100702.service.RetrofitListener;
import cn.dnui_yjw702.dnuifood_16110100702.service.OrderCall;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderModel extends BaseModel<OrderCall> implements OrderIface {

    @Override
    public void getAllOrdersByUser(Integer user_id, final RetrofitListener<List<CommentsEntity>> m) {
        Call<List<CommentsEntity>> allOrdersByUser = call.getAllOrdersByUser(user_id);
        allOrdersByUser.enqueue(new Callback<List<CommentsEntity>>() {
            @Override
            public void onResponse(Call<List<CommentsEntity>> call, Response<List<CommentsEntity>> response) {

                if (response.body() != null)
                    m.onSuccess(response.body());
                else
                    m.onFailure("获取数据失败");
            }

            @Override
            public void onFailure(Call<List<CommentsEntity>> call, Throwable t) {
                m.onFailure(t.toString());
            }
        });
    }

    @Override
    public void insertOrder(Integer user_id, Integer food_id, Integer num, Double sum, String suggesttime, String address, final RetrofitListener<Result> mCallback) {

        Call<Result> insertOrder = call.insertOrder(user_id, food_id, num, sum, suggesttime, address);
        insertOrder.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.body() != null) {
                    mCallback.onSuccess(response.body());
                }
                {
                    mCallback.onFailure("获取数据失败");
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                mCallback.onFailure(t.toString());
            }
        });
    }

    @Override
    public void insertComment(String item_id, String content, final RetrofitListener<Result> m) {
        Call<Result> insertComment = call.insertComment(item_id, content);
        insertComment.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.body() != null) {
                    m.onSuccess(response.body());
                } else {
                    m.onFailure("获取数据失败");
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                m.onFailure(t.toString());
            }
        });
    }

    @Override
    public void getAllCommentsByUser(Integer user_id, final RetrofitListener<List<CommentsEntity>> m) {
        Call<List<CommentsEntity>> getAllComments = call.getAllCommentsByUser(user_id);
        getAllComments.enqueue(new Callback<List<CommentsEntity>>() {
            @Override
            public void onResponse(Call<List<CommentsEntity>> call, Response<List<CommentsEntity>> response) {
                if (response.body() != null) {
                    m.onSuccess(response.body());
                } else {
                    m.onFailure("数据获取失败");
                }

            }

            @Override
            public void onFailure(Call<List<CommentsEntity>> call, Throwable t) {
                m.onFailure(t.toString());
            }
        });
    }

    @Override
    public void updateComment(String item_id, String content, final RetrofitListener<Result> m) {
        Call<Result> successEntityCall = call.updateComment(item_id, content);
        successEntityCall.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.body() != null) {
                    m.onSuccess(response.body());
                } else {
                    m.onFailure("获取失败");
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                m.onFailure(t.toString());
            }
        });
    }

    @Override
    public void deleteComment(String item_id, final RetrofitListener<Result> m) {
        Call<Result> successEntityCall = call.deleteComment(item_id);
        successEntityCall.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.body() != null) {
                    m.onSuccess(response.body());
                } else {
                    m.onFailure("获取数据失败");
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                m.onFailure(t.toString());
            }
        });
    }
}
