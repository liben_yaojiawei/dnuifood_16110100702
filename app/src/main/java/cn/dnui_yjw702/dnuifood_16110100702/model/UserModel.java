package cn.dnui_yjw702.dnuifood_16110100702.model;

import android.util.Log;

import cn.dnui_yjw702.dnuifood_16110100702.bean.LoginBean;
import cn.dnui_yjw702.dnuifood_16110100702.bean.Result;
import cn.dnui_yjw702.dnuifood_16110100702.bean.User;
import cn.dnui_yjw702.dnuifood_16110100702.iface.UserIface;
import cn.dnui_yjw702.dnuifood_16110100702.service.RetrofitListener;
import cn.dnui_yjw702.dnuifood_16110100702.service.UserService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserModel extends BaseModel<UserService> implements UserIface {
    //发起网络请求
    public void login(String username , String password, final RetrofitListener<LoginBean> callback){

        Call<LoginBean> login = call.login(username, password);

        login.enqueue(new Callback<LoginBean>() {
            //异步网络请求
            @Override
            public void onResponse(Call<LoginBean> call, Response<LoginBean> response) {
                if (response.body() != null) {
                    callback.onSuccess(response.body());
                }else{
                    callback.onFailure("数据为空");
                }
            }

            @Override
            public void onFailure(Call<LoginBean> call, Throwable t) {
                callback.onFailure(t.getMessage());
            }
        });
    }

    @Override
    public void register(String username, String password, String phone, String address, String comment,final RetrofitListener<Result> out) {


        final Call<Result> register = call.register(username, password,phone,address,comment);

        register.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                Log.d(TAG, "onResponse: " + response.body().getSuccess());
                if (response.body() != null) {
                    out.onSuccess(response.body());
                }else{
                    out.onFailure("数据为空");
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                out.onFailure(t.getMessage());
            }
        });
    }

    @Override
    public void getUser(Integer user_id,final RetrofitListener<User> out) {
        Call<User> user = call.getUser(user_id);
        user.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                Log.d(TAG, "onResponse: " + response.body().getUsername());
                if (response.body() != null) {
                    out.onSuccess(response.body());
                }else{
                    out.onFailure("数据为空");
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });
    }


    @Override
    public void updateUserById(Integer user_id, String username, String userpass, String mobilenum, String address,final RetrofitListener<Result> out) {
        Call<Result> user = call.updateUserById(user_id,username,userpass,mobilenum,address);
        user.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                Log.d(TAG, "onResponse: " + response.body().getSuccess());
                if (response.body() != null) {
                    out.onSuccess(response.body());
                }else{
                    out.onFailure("数据为空");
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {

            }
        });
    }

    private static final String TAG = "UserModel";
}