package cn.dnui_yjw702.dnuifood_16110100702.model;

import cn.dnui_yjw702.dnuifood_16110100702.bean.CommentsEntity;
import cn.dnui_yjw702.dnuifood_16110100702.bean.FoodMenu;
import cn.dnui_yjw702.dnuifood_16110100702.bean.Shop;
import cn.dnui_yjw702.dnuifood_16110100702.service.ShopService;
import cn.dnui_yjw702.dnuifood_16110100702.iface.FoodIface;
import cn.dnui_yjw702.dnuifood_16110100702.service.RetrofitListener;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FoodModel extends BaseModel<ShopService> implements FoodIface {
    private static final String TAG = "FoodModel";
    @Override
    public void getAllShops(final RetrofitListener<List<Shop>> out) {

        Call<List<Shop>> get = call.getAllShops();

        get.enqueue(new Callback<List<Shop>>() {
            @Override
            public void onResponse(Call<List<Shop>> call, Response<List<Shop>> response) {
                if (response.body() != null) {
                    out.onSuccess(response.body());
                }else if (response.body() != null&&response.body().size()==0){
                    out.onFailure("数据为空");
                }
            }

            @Override
            public void onFailure(Call<List<Shop>> call, Throwable t) {
                out.onFailure(t.getMessage());
            }
        });
    }

    @Override
    public void getFoodBySearch(String kw,final RetrofitListener<List<FoodMenu>> out) {
        Call<List<FoodMenu>> get = call.getFoodBySearch(kw);

        get.enqueue(new Callback<List<FoodMenu>>() {
            @Override
            public void onResponse(Call<List<FoodMenu>> call, Response<List<FoodMenu>> response) {
                if (response.body() != null) {
                    out.onSuccess(response.body());
                }else if(response.body() != null&&response.body().size()==0){
                    out.onFailure("数据为空");
                }
            }

            @Override
            public void onFailure(Call<List<FoodMenu>> call, Throwable t) {
                out.onFailure(t.getMessage());
            }
        });
    }

    @Override
    public void getAllCommentsByFood(String food_id, final RetrofitListener<List<CommentsEntity>> mCallback) {
        Call<List<CommentsEntity>> allCommentsByFood = call.getAllCommentsByFood(Integer.parseInt(food_id));
        allCommentsByFood.enqueue(new Callback<List<CommentsEntity>>() {
            @Override
            public void onResponse(Call<List<CommentsEntity>> call, Response<List<CommentsEntity>> response) {
                if (response.body()!=null&&response.body().size()!=0){
                    mCallback.onSuccess(response.body());
                }else if (response.body()==null){
                    mCallback.onFailure("获取数据失败");
                }else if(response.body().size()==0){
                    mCallback.onFailure("评论为空");
                }
            }

            @Override
            public void onFailure(Call<List<CommentsEntity>> call, Throwable t) {
                mCallback.onFailure(t.toString());
            }
        });
    }

    @Override
    public void getFoodById(Integer food_id, final RetrofitListener<FoodMenu> menuMCallback) {
        Call<FoodMenu> foodMenuCall = call.getFoodById(food_id);
        foodMenuCall.enqueue(new Callback<FoodMenu>() {
            @Override
            public void onResponse(Call<FoodMenu> call, Response<FoodMenu> response) {
                if (response.body()!=null){
                    menuMCallback.onSuccess(response.body());
                }else{
                    menuMCallback.onFailure("获取失败");
                }
            }

            @Override
            public void onFailure(Call<FoodMenu> call, Throwable t) {
                menuMCallback.onFailure(t.toString());
            }
        });
    }
}
