package cn.dnui_yjw702.dnuifood_16110100702.model;

import java.util.List;

import cn.dnui_yjw702.dnuifood_16110100702.bean.CollectStatus;
import cn.dnui_yjw702.dnuifood_16110100702.bean.IsCollection;
import cn.dnui_yjw702.dnuifood_16110100702.bean.Result;
import cn.dnui_yjw702.dnuifood_16110100702.iface.CollectionIface;
import cn.dnui_yjw702.dnuifood_16110100702.service.CollectService;
import cn.dnui_yjw702.dnuifood_16110100702.service.RetrofitListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CollectionModel extends BaseModel<CollectService> implements CollectionIface {

    @Override
    public void getAllUserCollection(Integer user_id, Integer flag, final RetrofitListener<List<CollectStatus>> mCallback) {
        Call<List<CollectStatus>> allUserCollection = call.getAllUserCollection(user_id, flag);

        allUserCollection.enqueue(new Callback<List<CollectStatus>>() {
            @Override
            public void onResponse(Call<List<CollectStatus>> call, Response<List<CollectStatus>> response) {


                if (response.body()!=null){
                    mCallback.onSuccess(response.body());
                }else if (response.body() != null&&response.body().size()==0){
                    mCallback.onFailure("收藏为空");
                }else{
                    mCallback.onFailure("获取收藏失败");
                }
            }

            @Override
            public void onFailure(Call<List<CollectStatus>> call, Throwable t) {
                mCallback.onFailure("获取收藏失败:" + t.toString());
            }
        });
    }

    @Override
    public void isCollected(Integer user_id, Integer shop_food_id, Integer flag, final RetrofitListener<IsCollection> mCallback) {
        Call<IsCollection> isCollectionCall = call.isCollected(user_id,shop_food_id,flag);
        isCollectionCall.enqueue(new Callback<IsCollection>() {
            @Override
            public void onResponse(Call<IsCollection> call, Response<IsCollection> response) {
                if (response.body()!=null){
                    mCallback.onSuccess(response.body());
                }else {
                    mCallback.onFailure("获取失败");
                }
            }

            @Override
            public void onFailure(Call<IsCollection> call, Throwable t) {
                mCallback.onFailure(t.toString());
            }
        });
    }

    @Override
    public void userCollectFood(Integer user_id, Integer food_id,final RetrofitListener<Result> mCallback) {

        Call<Result> collected = call.userCollectFood(user_id,food_id);
        collected.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.body()!=null){
                    mCallback.onSuccess(response.body());
                }else {
                    mCallback.onFailure("获取失败");
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                mCallback.onFailure(t.toString());
            }
        });

    }

    @Override
    public void userCollectShop(Integer user_id, Integer shop_id,final RetrofitListener<Result> mCallback) {
        Call<Result> collected = call.userCollectShop(user_id,shop_id);

        collected.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.body()!=null){
                    mCallback.onSuccess(response.body());
                }else {
                    mCallback.onFailure("获取失败");
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                mCallback.onFailure(t.toString());
            }
        });
    }
}
