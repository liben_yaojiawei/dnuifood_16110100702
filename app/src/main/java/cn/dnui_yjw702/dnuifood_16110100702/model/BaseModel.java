package cn.dnui_yjw702.dnuifood_16110100702.model;

import java.lang.reflect.ParameterizedType;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BaseModel<T> {
    //创建Retrofit对象
    Retrofit retrofit;
    public Class<T> getTClass()
    {   //获取接口服务对象
        Class<T> tClass = (Class<T>)((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        return tClass;
    }

    public BaseModel(){
        retrofit = new Retrofit.Builder()
                .baseUrl("http://172.24.10.175:8080/foodService/") //设置网络请求的Url地址
                .addConverterFactory(GsonConverterFactory.create()) //设置数据解析器
                .build();
        call = retrofit.create(getTClass());//创建实例  通过retrofit和定义的有网络访问方法的接口关联
    }
    public T call;
}
