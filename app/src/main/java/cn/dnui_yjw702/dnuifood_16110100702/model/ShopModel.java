package cn.dnui_yjw702.dnuifood_16110100702.model;

import java.util.List;

import cn.dnui_yjw702.dnuifood_16110100702.bean.FoodMenu;
import cn.dnui_yjw702.dnuifood_16110100702.iface.ShopIface;
import cn.dnui_yjw702.dnuifood_16110100702.service.RetrofitListener;
import cn.dnui_yjw702.dnuifood_16110100702.service.ShopService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShopModel extends BaseModel<ShopService> implements ShopIface {
    //菜谱列表
    @Override
    public void getFoodByShop(Integer shop_id, final RetrofitListener<List<FoodMenu>> out) {
        Call<List<FoodMenu>> foodByShop = call.getFoodByShop(shop_id);
        foodByShop.enqueue(new Callback<List<FoodMenu>>() {
            @Override
            public void onResponse(Call<List<FoodMenu>> call, Response<List<FoodMenu>> response) {
                if (response.body() != null)
                    out.onSuccess(response.body());
                else
                    out.onFailure("获取失败");
            }

            @Override
            public void onFailure(Call<List<FoodMenu>> call, Throwable t) {
                out.onFailure(t.toString());
            }
        });
    }
}
