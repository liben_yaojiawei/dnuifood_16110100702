package cn.dnui_yjw702.dnuifood_16110100702.adapter;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import cn.dnui_yjw702.dnuifood_16110100702.MyCompent.Url;
import cn.dnui_yjw702.dnuifood_16110100702.R;

import cn.dnui_yjw702.dnuifood_16110100702.activity.FoodDetailActivity;
import cn.dnui_yjw702.dnuifood_16110100702.bean.FoodMenu;

public class FoodListAdapter extends RecyclerView.Adapter<FoodListAdapter.ViewHolder>{
    private List<FoodMenu> foods;
    private View view;

    public FoodListAdapter(List<FoodMenu> foods){
        this.foods=foods;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView foodImage;
        TextView foodName;
        TextView foodIntro;
        TextView price;
        public ViewHolder(View itemView) {
            super(itemView);
            foodImage = itemView.findViewById(R.id.foodImage);
            foodName = itemView.findViewById(R.id.foodName);
            foodIntro = itemView.findViewById(R.id.foodIntro);
            price = itemView.findViewById(R.id.foodPrice);
        }
    }

    @NonNull
    @Override
    public FoodListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.food_item,parent,false);

        ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(final FoodListAdapter.ViewHolder holder, int position){
        final FoodMenu foodMenu=foods.get(position);//获取点击的条目的位置

        holder.price.setText(foodMenu.getPrice()+"元");
        holder.foodIntro.setText(foodMenu.getIntro());
        holder.foodName.setText(foodMenu.getFoodname());
        Glide.with(view.getContext()).load(Url.imgBase+foodMenu.getPic()).into(holder.foodImage);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.getContext().startActivity(new Intent(v.getContext(),
                        FoodDetailActivity.class).putExtra("food",foodMenu));
            }
        });


    }

    @Override
    public int getItemCount() {
        return foods.size();
    }

}
