package cn.dnui_yjw702.dnuifood_16110100702.adapter;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import cn.dnui_yjw702.dnuifood_16110100702.MyCompent.Url;
import cn.dnui_yjw702.dnuifood_16110100702.R;
import cn.dnui_yjw702.dnuifood_16110100702.activity.FoodListActivity;
import cn.dnui_yjw702.dnuifood_16110100702.bean.CollectStatus;

public class CollectShopAdapter extends RecyclerView.Adapter<CollectShopAdapter.ViewHolder> {

    private View view;
    private List<CollectStatus> shops;

    public CollectShopAdapter(List<CollectStatus> shops) {
        this.shops = shops;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.collect_shop_item,parent,false);

        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final CollectStatus shop = shops.get(position);

        holder.address.setText(shop.getAddress());
        holder.shopName.setText(shop.getShopname());
        Glide.with(view.getContext()).load(Url.imgBase+shop.getPic()).into(holder.shopImage);
        holder.enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view.getContext().startActivity(
                        new Intent(view.getContext(), FoodListActivity.class)   //跳转到店铺列表
                                .putExtra("shop_id",shop.getShop_id())
                                .putExtra("shopName",shop.getShopname()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return shops.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView shopName,address;
        ImageView shopImage;
        Button enter,cancel;
        public ViewHolder(View itemView) {
            super(itemView);
            shopImage = itemView.findViewById(R.id.shopImage);
            shopName = itemView.findViewById(R.id.shopName);
            address = itemView.findViewById(R.id.address);
            enter = itemView.findViewById(R.id.enter);
            cancel = itemView.findViewById(R.id.cancel);
        }
    }
}
