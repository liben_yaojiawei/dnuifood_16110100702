package cn.dnui_yjw702.dnuifood_16110100702.adapter;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;

import cn.dnui_yjw702.dnuifood_16110100702.MyCompent.Url;
import cn.dnui_yjw702.dnuifood_16110100702.R;
import cn.dnui_yjw702.dnuifood_16110100702.activity.FoodDetailActivity;
import cn.dnui_yjw702.dnuifood_16110100702.bean.CollectStatus;
import cn.dnui_yjw702.dnuifood_16110100702.bean.FoodMenu;
import cn.dnui_yjw702.dnuifood_16110100702.model.FoodModel;
import cn.dnui_yjw702.dnuifood_16110100702.service.RetrofitListener;

public class CollectFoodAdapter extends RecyclerView.Adapter<CollectFoodAdapter.ViewHolder> {
    private List<CollectStatus> foods;

    public CollectFoodAdapter(List<CollectStatus> foods) {
        this.foods = foods;
    }
    private View view;
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.collect_food_item,parent,false);

        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final CollectStatus food = foods.get(position);

        holder.price.setText(food.getPrice()+"元");
        holder.foodName.setText(food.getFoodname());
        Glide.with(view.getContext()).load(Url.imgBase+food.getPic()).into(holder.shopImage);
        holder.enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FoodModel model = new FoodModel();
                model.getFoodById(food.getFood_id(), new RetrofitListener<FoodMenu>() {
                    @Override
                    public void onSuccess(FoodMenu data) {
                        if (data!=null){

                            view.getContext().startActivity(
                                    new Intent(view.getContext(), FoodDetailActivity.class)    //菜品详情页
                                            .putExtra("food",data));
                        }else{
                            Toast.makeText(view.getContext(),"获取失败",Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(String error) {
                        Toast.makeText(view.getContext(),error,Toast.LENGTH_SHORT).show();
                    }
                });


            }
        });
    }

    @Override
    public int getItemCount() {
        return foods.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView foodName,price;
        ImageView shopImage;
        Button enter,cancel;
        public ViewHolder(View itemView) {
            super(itemView);
            foodName = itemView.findViewById(R.id.foodName);
            price = itemView.findViewById(R.id.price);
            shopImage =itemView.findViewById(R.id.shopImage);
            enter = itemView.findViewById(R.id.enter);
            cancel = itemView.findViewById(R.id.cancel);
        }
    }
}
