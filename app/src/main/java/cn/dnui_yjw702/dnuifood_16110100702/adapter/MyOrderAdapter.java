package cn.dnui_yjw702.dnuifood_16110100702.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.List;

import cn.dnui_yjw702.dnuifood_16110100702.R;
import cn.dnui_yjw702.dnuifood_16110100702.bean.CommentsEntity;
import cn.dnui_yjw702.dnuifood_16110100702.bean.Result;
import cn.dnui_yjw702.dnuifood_16110100702.model.OrderModel;
import cn.dnui_yjw702.dnuifood_16110100702.service.RetrofitListener;

public class MyOrderAdapter extends RecyclerView.Adapter<MyOrderAdapter.ViewHolder> {

    private static final String TAG = "MyOrderAdapter";
    OrderModel model = new OrderModel();
    private List<CommentsEntity> orders;
    private View view;
    private String resp;

    public MyOrderAdapter(List<CommentsEntity> orders) {
        this.orders = orders;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_item, parent, false);

        ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CommentsEntity order = orders.get(position);

        holder.comment.setOnClickListener(new MyClick(order, holder.comment));
        if (order.getContent() != null && order.getContent().length() != 0) {
            holder.comment.setText("再次评价");
        }

        holder.shopName.setText(order.getShopname());
        holder.foodName.setText(order.getFoodname());
        holder.buyTime.setText(order.getOrder_time());
        holder.sum.setText(order.getSum() + "");
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView foodName, buyTime;
        TextView shopName, sum;
        Button comment;

        public ViewHolder(View itemView) {
            super(itemView);
            foodName = itemView.findViewById(R.id.foodName);
            buyTime = itemView.findViewById(R.id.buyTime);
            shopName = itemView.findViewById(R.id.shopName);
            sum = itemView.findViewById(R.id.sum);
            comment = itemView.findViewById(R.id.comment);
        }
    }

    class MyClick implements View.OnClickListener {
        public CommentsEntity order;
        public Button button;

        public MyClick(CommentsEntity order, Button button) {
            this.order = order;
            this.button = button;
        }

        @Override
        public void onClick(final View v) {
            switch (v.getId()) {
                case R.id.comment:
                    new MaterialDialog.Builder(v.getContext())
                            .title("评论")
                            .inputType(InputType.TYPE_CLASS_TEXT)
                            .input("请输入评论", null, new MaterialDialog.InputCallback() {
                                @Override
                                public void onInput(MaterialDialog dialog, final CharSequence input) {

                                    model.insertComment(order.getItem_id() + "", input.toString(), new RetrofitListener<Result>() {
                                        @Override
                                        public void onSuccess(Result data) {

                                            if (data.getSuccess() == 1) {
                                                Toast.makeText(v.getContext(), "评价成功", Toast.LENGTH_SHORT).show();
                                                button.setText("再次评价");
                                            } else {
                                                Toast.makeText(v.getContext(), "评价失败", Toast.LENGTH_SHORT).show();
                                            }
                                        }

                                        @Override
                                        public void onFailure(String error) {

                                        }
                                    });
                                }
                            }).show();

                    break;
            }
        }
    }
}
