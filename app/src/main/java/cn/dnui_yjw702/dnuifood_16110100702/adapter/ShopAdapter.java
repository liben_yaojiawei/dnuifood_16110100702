package cn.dnui_yjw702.dnuifood_16110100702.adapter;


import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import cn.dnui_yjw702.dnuifood_16110100702.MyCompent.Url;
import cn.dnui_yjw702.dnuifood_16110100702.R;

import cn.dnui_yjw702.dnuifood_16110100702.activity.FoodListActivity;
import cn.dnui_yjw702.dnuifood_16110100702.bean.Shop;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;

public class ShopAdapter extends RecyclerView.Adapter<ShopAdapter.ViewHolder>{
    private List<Shop> shops;
    private View view;
    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView shopImage;
        TextView shopName;
        MaterialRatingBar level;
        TextView address;
        public ViewHolder(View itemView) {
            super(itemView);
            shopImage = itemView.findViewById(R.id.shopImage);
            shopName = itemView.findViewById(R.id.shopName);
            level = itemView.findViewById(R.id.level);
            address = itemView.findViewById(R.id.address);
        }
    }
    public ShopAdapter(List<Shop> shopEntities){
        this.shops=shopEntities;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.shop_item,parent,false);
        //绑定item
        ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position){
        final Shop shop =shops.get(position);
        //条目点击事件，跳转到店铺详情
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view.getContext().startActivity(
                        new Intent(view.getContext(), FoodListActivity.class)
                                .putExtra("shop_id", shop.getShop_id())
                                .putExtra("shopName", shop.getShopname()));//点击跳转时把id和name传递
            }
        });
        holder.address.setText(shop.getAddress());
        holder.shopName.setText(shop.getShopname());
        holder.shopImage.setImageResource(shop.getImageRes());
        holder.level.setRating(shop.getLevel());
        Glide.with(view.getContext()).load(Url.imgBase+ shop.getPic()).into(holder.shopImage);
    }

    @Override
    public int getItemCount() {
        return shops.size();
    }

}
