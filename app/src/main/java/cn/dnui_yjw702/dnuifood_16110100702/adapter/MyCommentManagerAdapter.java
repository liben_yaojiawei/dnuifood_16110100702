package cn.dnui_yjw702.dnuifood_16110100702.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.List;

import cn.dnui_yjw702.dnuifood_16110100702.R;
import cn.dnui_yjw702.dnuifood_16110100702.bean.CommentsEntity;
import cn.dnui_yjw702.dnuifood_16110100702.bean.Result;
import cn.dnui_yjw702.dnuifood_16110100702.model.OrderModel;
import cn.dnui_yjw702.dnuifood_16110100702.service.RetrofitListener;

public class MyCommentManagerAdapter extends RecyclerView.Adapter<MyCommentManagerAdapter.ViewHolder> {
    //我的评论列表
    private static final String TAG = "MyCommentManagerAdapter";
    OrderModel model = new OrderModel();
    private List<CommentsEntity> orders;
    private View view;
    private String resp;

    public MyCommentManagerAdapter(List<CommentsEntity> orders) {
        this.orders = orders;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.comment_manage_item, parent, false);

        ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder
                                         holder, int position) {
        CommentsEntity order = orders.get(position);
        holder.foodName.setText(order.getFoodname());
        holder.shopName.setText(order.getShopname());
        holder.time.setText(order.getComment_time());

        holder.comment.setText(order.getContent());
        MyClick myClick = new MyClick(order);
        holder.modify.setOnClickListener(myClick);
        holder.delete.setOnClickListener(myClick);
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    private void modifyComment(final CommentsEntity order, final View v) {

        //todo:modifyComment
        new MaterialDialog.Builder(v.getContext())
                .title("修改评论")
                .inputType(InputType.TYPE_CLASS_TEXT)
                .input("请输入评论", null, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog dialog, final CharSequence input) {

                        model.updateComment(order.getItem_id() + "", input.toString(), new RetrofitListener<Result>() {
                            @Override
                            public void onSuccess(Result data) {

                                if (data.getSuccess() == 1) {
                                    order.setContent(input.toString());
                                    notifyDataSetChanged();
                                    Toast.makeText(v.getContext(), "修改成功", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(v.getContext(), "修改失败", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(String error) {
                                Toast.makeText(v.getContext(), "修改失败:" + error, Toast.LENGTH_SHORT).show();
                            }
                        });


                    }
                }).show();
    }

    private void deleteComment(final CommentsEntity order, View v) {

        //todo:delete

        model.deleteComment(order.getItem_id() + "", new RetrofitListener<Result>() {
            @Override
            public void onSuccess(Result data) {

                if (data.getSuccess()==1){
                    orders.remove(order);
                    notifyDataSetChanged();
                    Toast.makeText(view.getContext(), "删除成功", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(view.getContext(), "删除失败", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(String error) {
                Toast.makeText(view.getContext(), error, Toast.LENGTH_SHORT).show();
            }
        });

    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView foodName, comment, shopName, time;
        Button modify, delete;

        public ViewHolder(View itemView) {
            super(itemView);
            foodName = itemView.findViewById(R.id.foodName);
            comment = itemView.findViewById(R.id.comment);
            shopName = itemView.findViewById(R.id.shopName);
            time = itemView.findViewById(R.id.time);
            modify = itemView.findViewById(R.id.modify);
            delete = itemView.findViewById(R.id.delete);

        }
    }

    class MyClick implements View.OnClickListener {

        private CommentsEntity order;

        public MyClick(CommentsEntity order) {
            this.order = order;
        }

        @Override
        public void onClick(final View v) {
            switch (v.getId()) {
                case R.id.modify:
                    modifyComment(order, v);

                    break;
                case R.id.delete:
                    deleteComment(order, v);

                    break;
            }
        }
    }

}
