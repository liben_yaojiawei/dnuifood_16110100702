package cn.dnui_yjw702.dnuifood_16110100702.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;



import java.util.List;

import cn.dnui_yjw702.dnuifood_16110100702.R;
import cn.dnui_yjw702.dnuifood_16110100702.bean.CommentsEntity;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder>{
    //评论适配器
    private List<CommentsEntity> orders;

    private View view;
    public CommentAdapter(List<CommentsEntity> orders) {
        this.orders = orders;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.comment_item,parent,false);

        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    private static final String TAG = "CommentAdapter";
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CommentsEntity order=orders.get(position);
        Log.d(TAG, "onBindViewHolder: " +order);
        holder.comment.setText("评价："+order.getContent());
        holder.username.setText("用户："+order.getUsername());
        holder.buy_food.setText("购买菜品："+order.getFoodname());
        holder.order_id.setText("订单编号："+order.getOrder_id());
        holder.suggest_time.setText("建议送达："+order.getSuggesttime());
        holder.comment_time.setText("评价时间"+order.getComment_time());

    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView username,comment_time,order_id,suggest_time,buy_food,comment;


        public ViewHolder(View itemView) {
            super(itemView);
            username = itemView.findViewById(R.id.username);
            comment_time = itemView.findViewById(R.id.comment_time);
            order_id = itemView.findViewById(R.id.orderId);
            suggest_time = itemView.findViewById(R.id.suggest_time);
            buy_food = itemView.findViewById(R.id.buy_food);
            comment = itemView.findViewById(R.id.comment);
        }
    }
}
