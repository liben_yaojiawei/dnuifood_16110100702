package cn.dnui_yjw702.dnuifood_16110100702.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import cn.dnui_yjw702.dnuifood_16110100702.R;
import cn.dnui_yjw702.dnuifood_16110100702.adapter.FoodListAdapter;
import cn.dnui_yjw702.dnuifood_16110100702.bean.FoodMenu;
import cn.dnui_yjw702.dnuifood_16110100702.model.FoodModel;
import cn.dnui_yjw702.dnuifood_16110100702.service.RetrofitListener;

public class SearchFrament extends Fragment implements View.OnClickListener {

    private View view;
    private Button search_bt;
    private EditText search_et;
    private List<FoodMenu> foodMenus;
    private RecyclerView recy;
    private FoodListAdapter foodListAdapter;
    private Context context;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_search,container,false);
        search_bt = view.findViewById(R.id.search_bt);
        context = view.getContext();
        search_bt.setOnClickListener(this);
        search_et = view.findViewById(R.id.search_et);
        recy = view.findViewById(R.id.search_recy);
        foodMenus = new ArrayList<>();
        foodListAdapter = new FoodListAdapter(foodMenus);
        recy.setLayoutManager(new LinearLayoutManager(view.getContext()));
        recy.setAdapter(foodListAdapter);
                //创建时填充数据
                FoodModel model = new FoodModel();
                model.getFoodBySearch(search_et.getText().toString(), new RetrofitListener<List<FoodMenu>>() {
                    @Override
                    public void onSuccess(List<FoodMenu> data) {
                        foodMenus.clear();
                        foodMenus.addAll(data);
                        foodListAdapter.notifyDataSetChanged();

                    }

                    @Override
                    public void onFailure(String error) {
                        Toast.makeText(context,error,Toast.LENGTH_SHORT).show();
                    }
                });
        return view;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.search_bt:
                FoodModel model = new FoodModel();
                model.getFoodBySearch(search_et.getText().toString(), new RetrofitListener<List<FoodMenu>>() {
                    @Override
                    public void onSuccess(List<FoodMenu> data) {
                        foodMenus.clear();
                        foodMenus.addAll(data);
                        foodListAdapter.notifyDataSetChanged();

                        Toast.makeText(context,"获取成功",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(String error) {
                        Toast.makeText(context,error,Toast.LENGTH_SHORT).show();
                    }
                });

                break;
        }
    }
}
