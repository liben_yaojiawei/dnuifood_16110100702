package cn.dnui_yjw702.dnuifood_16110100702.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import cn.dnui_yjw702.dnuifood_16110100702.R;
import cn.dnui_yjw702.dnuifood_16110100702.adapter.ShopAdapter;
import cn.dnui_yjw702.dnuifood_16110100702.bean.Shop;
import cn.dnui_yjw702.dnuifood_16110100702.model.FoodModel;
import cn.dnui_yjw702.dnuifood_16110100702.service.RetrofitListener;


public class HomeFragment extends Fragment {

    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private List<Shop> shops;
    private ShopAdapter shopAdapter;
    private Context context;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main,container,false);
        //设置布局
        recyclerView = view.findViewById(R.id.recy_lv);
        shops = new ArrayList<>();
        //设置动画效果
        layoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(layoutManager);
        //实例化shopAdapter
        shopAdapter = new ShopAdapter(shops);
        //设置适配器
        recyclerView.setAdapter(shopAdapter);

        context = getContext();
        loadData();
        return view;
    }

    public void loadData() {

        FoodModel model = new FoodModel();

        model.getAllShops(new RetrofitListener<List<Shop>>() {
            @Override
            public void onSuccess(List<Shop> data) {

                shops.clear();
                shops.addAll(data);//添加数据
                shopAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(String error) {
                Toast.makeText(context,error,Toast.LENGTH_SHORT).show();
            }
        });
    }

}
