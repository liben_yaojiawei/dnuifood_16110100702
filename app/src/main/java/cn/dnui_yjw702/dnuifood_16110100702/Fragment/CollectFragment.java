package cn.dnui_yjw702.dnuifood_16110100702.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import cn.dnui_yjw702.dnuifood_16110100702.R;

public class CollectFragment extends Fragment implements View.OnClickListener{
    private Button shop,food;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_collect,container,false);

        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.replace, new ShopCollectFragment()).commit();

        shop = view.findViewById(R.id.shop);
        food = view.findViewById(R.id.food);
        shop.setOnClickListener(this);
        food.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.shop:

                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.replace, new ShopCollectFragment()).commit();

                break;
            case R.id.food:

                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.replace, new FoodCollectFragment()).commit();

                break;
        }
    }
}
