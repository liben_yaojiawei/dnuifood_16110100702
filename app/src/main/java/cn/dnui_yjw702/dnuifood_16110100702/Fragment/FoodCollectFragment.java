package cn.dnui_yjw702.dnuifood_16110100702.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import cn.dnui_yjw702.dnuifood_16110100702.R;
import cn.dnui_yjw702.dnuifood_16110100702.adapter.CollectFoodAdapter;
import cn.dnui_yjw702.dnuifood_16110100702.bean.CollectStatus;
import cn.dnui_yjw702.dnuifood_16110100702.bean.Store;
import cn.dnui_yjw702.dnuifood_16110100702.model.CollectionModel;
import cn.dnui_yjw702.dnuifood_16110100702.service.RetrofitListener;

public class FoodCollectFragment extends Fragment{

    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private List<CollectStatus> foods;
    private CollectFoodAdapter adapter;
    private Context context;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_collect_food,container,false);
        recyclerView = view.findViewById(R.id.food_recy);
        context = view.getContext();
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        foods = new ArrayList<>();
        adapter = new CollectFoodAdapter(foods);
        recyclerView.setAdapter(adapter);

        CollectionModel model = new CollectionModel();
        model.getAllUserCollection(Integer.parseInt(Store.userId), 1, new RetrofitListener<List<CollectStatus>>() {
            @Override
            public void onSuccess(List<CollectStatus> data) {
                if (data!=null&&data.size()!=0){
                    foods.clear();
                    foods.addAll(data);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(String error) {
                Toast.makeText(context,error,Toast.LENGTH_SHORT).show();
            }
        });


        return view;
    }

    private static final String TAG = "FoodCollectFragment";
}
