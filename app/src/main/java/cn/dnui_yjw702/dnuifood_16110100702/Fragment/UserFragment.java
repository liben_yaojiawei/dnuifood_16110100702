package cn.dnui_yjw702.dnuifood_16110100702.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cn.dnui_yjw702.dnuifood_16110100702.R;
import cn.dnui_yjw702.dnuifood_16110100702.activity.MyCommentActivity;
import cn.dnui_yjw702.dnuifood_16110100702.activity.MyOrderActivity;
import cn.dnui_yjw702.dnuifood_16110100702.activity.SettingActivity;
import cn.dnui_yjw702.dnuifood_16110100702.activity.UserModifyActivity;
import cn.dnui_yjw702.dnuifood_16110100702.bean.Store;

public class UserFragment extends Fragment implements View.OnClickListener {

    private TextView mycomment,myorder,username,phone,mycart;
    private RelativeLayout rl;
    private Context context;
    private ImageView setting;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user,container,false);
        init(view);
        context = getContext();
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mycomment:
                context.startActivity(new Intent(context, MyCommentActivity.class));
                break;

            case R.id.myorder:
                context.startActivity(new Intent(context, MyOrderActivity.class));
                break;

            case R.id.user_data:
                context.startActivity(new Intent(context, UserModifyActivity.class));
                break;

            case R.id.settting:
                context.startActivity(new Intent(context, SettingActivity.class));

                break;


        }
    }

    public void init(View view){
        username = view.findViewById(R.id.username);
        username.setText(Store.user.getUsername());
        phone = view.findViewById(R.id.phone);
        phone.setText(Store.user.getMobilenum());
        setting = view.findViewById(R.id.settting);
        setting.setOnClickListener(this);
        mycomment = view.findViewById(R.id.mycomment);
        myorder = view.findViewById(R.id.myorder);
        rl = view.findViewById(R.id.user_data);
        myorder.setOnClickListener(this);
        mycomment.setOnClickListener(this);
        rl.setOnClickListener(this);

    }

    @Override
    public void onResume() {
        super.onResume();
        username.setText(Store.user.getUsername());
        phone.setText(Store.user.getMobilenum());
    }
}
