package cn.dnui_yjw702.dnuifood_16110100702.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import cn.dnui_yjw702.dnuifood_16110100702.R;
import cn.dnui_yjw702.dnuifood_16110100702.adapter.CollectShopAdapter;
import cn.dnui_yjw702.dnuifood_16110100702.bean.CollectStatus;
import cn.dnui_yjw702.dnuifood_16110100702.bean.Store;
import cn.dnui_yjw702.dnuifood_16110100702.model.CollectionModel;
import cn.dnui_yjw702.dnuifood_16110100702.service.RetrofitListener;

public class ShopCollectFragment extends Fragment {
    private RecyclerView recyclerView;
    private CollectShopAdapter adapter;
    private List<CollectStatus> shops;
    private LinearLayoutManager layoutManager;
    private Context context;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_collect_shop,container,false);
        context = view.getContext();
        recyclerView = view.findViewById(R.id.shop_recy);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        shops = new ArrayList<>();

        CollectionModel model = new CollectionModel();
        model.getAllUserCollection(Integer.parseInt(Store.userId), 0, new RetrofitListener<List<CollectStatus>>() {
            @Override
            public void onSuccess(List<CollectStatus> data) {
                shops.clear();
                shops.addAll(data);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(String error) {
                Toast.makeText(context,error,Toast.LENGTH_SHORT).show();
            }
        });



        adapter = new CollectShopAdapter(shops);
        recyclerView.setAdapter(adapter);
        return view;
    }

}
